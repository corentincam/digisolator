// Digispark constants
var digispark_l1 = 15;
var digispark_w1 = 12;
var digispark_l2 = 10;
var digispark_w2 = 5;
var digispark_h = 2;

// LED constants
var led_h = 7;
var led_d = 5;

// Switch constants
var switch_h = 15;
var switch_d = 5;

// Board constants
var board_l = 90;
var board_w = 60;
var board_h = 1;
var holes_r = 1.5;
var holes_offset = 3;

// Offsets
var digi_offset_x = 40;
var digi_offset_y = 0;
var digi_offset_z = 15;
var led_offset_x = 60;
var led_offset_y = 15;
var led_offset_z = 3;
var switch_offset_x = 10;
var switch_offset_y = 50;
var switch_offset_z = 6;

// Box constants
var box_l = board_l + 10;
var box_w = board_w + 10;
var box_h = board_h + digispark_h + digi_offset_z + 10;
var box_r = 5;
var wires_hole_offset_y = 25;
var wires_hole_d = 10;

function digispark() {
    return union(
        cube({
            center: [0, 0, 0],
            size: [digispark_l1, digispark_w1, digispark_h]
        }),
        cube({
            center: [0, 0, 0],
            size: [digispark_l2, digispark_w2, digispark_h]
        }).translate([digispark_l1/2 - digispark_l2/2, -digispark_w2, 0])
    );
}

function led() {
    return union(
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, led_h/5],
            radiusStart: led_d/2,
            radiusEnd: led_d/2,
            resolution: 16}),
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, led_h - led_d/2],
            radiusStart: led_d/2.2,
            radiusEnd: led_d/2.2,
            resolution: 16}),
        sphere({
            center: [0, 0, 0],
            r: led_d/2.2,
            fn: 16}).translate([-led_d/2.2, -led_d/2.2, led_h-led_d])
    );
}

function power_switch() {
    return union(
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, switch_h],
            radiusStart: switch_d/2,
            radiusEnd: switch_d/2,
            resolution: 16}),
        CSG.cylinder({
            start: [0, 0, switch_h],
            end: [0, 0, switch_h*2-switch_d/4],
            radiusStart: switch_d/4,
            radiusEnd: switch_d/4,
            resolution: 16}),
        sphere({
            center: [0, 0, 0],
            r: switch_d/4,
            fn: 16}).translate([-switch_d/4, -switch_d/4, switch_h*2-switch_d/2])
    ).rotateX(90).rotateZ(270);
}

function board() {
    return difference(
        cube({
            center: [0, 0, 0],
            size: [board_l, board_w, board_h]
        })
        ,
        union(
            CSG.cylinder({
                start: [holes_offset, holes_offset, 0],
                end: [holes_offset, holes_offset, board_h],
                radiusStart: holes_r,
                radiusEnd: holes_r,
                resolution: 16}),
            CSG.cylinder({
                start: [board_l - holes_offset, holes_offset, 0],
                end: [board_l - holes_offset, holes_offset, board_h],
                radiusStart: holes_r,
                radiusEnd: holes_r,
                resolution: 16}),
            CSG.cylinder({
                start: [holes_offset, board_w - holes_offset, 0],
                end: [holes_offset, board_w - holes_offset, board_h],
                radiusStart: holes_r,
                radiusEnd: holes_r,
                resolution: 16}),
            CSG.cylinder({
                start: [board_l - holes_offset, board_w - holes_offset, 0],
                end: [board_l - holes_offset, board_w - holes_offset, board_h],
                radiusStart: holes_r,
                radiusEnd: holes_r,
                resolution: 16})
        )
    );
}

// Complete isolator
function isolator() {
    return union(
        board(),
        digispark().translate([digi_offset_x, digi_offset_y, digi_offset_z]),
        led().translate([led_offset_x, led_offset_y, led_offset_z]),
        power_switch().translate([switch_offset_x, switch_offset_y, switch_offset_z])
    );
}

function box() {
    return difference(
        union(
            // Base
            difference(
                cube({
                    center: [0, 0, 0],
                    size: [box_l, box_w, box_h],
                    radius: box_r,
                    round: true,
                    fn: 32
                }),
                cube({
                    center: [0, 0, 0],
                    size: [box_l-4, box_w-4, box_h-4],
                    radius: box_r-2,
                    round: true,
                    fn: 32
                }).translate([2, 2, 2])
            ),

            // Board supports
            CSG.cylinder({
                start: [0, 0, 0],
                end: [0, 0, box_h],
                radiusStart: holes_r+2,
                radiusEnd: holes_r+2,
                resolution: 16}).translate([holes_offset+5, holes_offset+5, 0]),
            CSG.cylinder({
                start: [0, 0, 0],
                end: [0, 0, box_h],
                radiusStart: holes_r+2,
                radiusEnd: holes_r+2,
                resolution: 16}).translate([board_l-holes_offset+5, holes_offset+5, 0]),
            CSG.cylinder({
                start: [0, 0, 0],
                end: [0, 0, box_h],
                radiusStart: holes_r+2,
                radiusEnd: holes_r+2,
                resolution: 16}).translate([holes_offset+5, board_w-holes_offset+5, 0]),
            CSG.cylinder({
                start: [0, 0, 0],
                end: [0, 0, box_h],
                radiusStart: holes_r+2,
                radiusEnd: holes_r+2,
                resolution: 16}).translate([board_l-holes_offset+5, board_w-holes_offset+5, 0])
        ),

        // Screws holes
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, box_h-2],
            radiusStart: holes_r-0.1,
            radiusEnd: holes_r-0.1,
            resolution: 16}).translate([holes_offset+5, holes_offset+5, 0]),
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, box_h-2],
            radiusStart: holes_r-0.1,
            radiusEnd: holes_r-0.1,
            resolution: 16}).translate([board_l-holes_offset+5, holes_offset+5, 0]),
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, box_h-2],
            radiusStart: holes_r-0.1,
            radiusEnd: holes_r-0.1,
            resolution: 16}).translate([holes_offset+5, board_w-holes_offset+5, 0]),
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, box_h-2],
            radiusStart: holes_r-0.1,
            radiusEnd: holes_r-0.1,
            resolution: 16}).translate([board_l-holes_offset+5, board_w-holes_offset+5, 0]),

        // Digispark hole
        cube({
            center: [0, 0, 0],
            size: [digispark_l1+2, digispark_w1+7, box_h - digi_offset_z]
        }).translate([digi_offset_x+4, 0, digi_offset_z + box_h*0.2 - 5]),

        // LED hole
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, 2],
            radiusStart: led_d/2+0.5,
            radiusEnd: led_d/2+0.5,
            resolution: 16}).translate([led_offset_x+5, led_offset_y+5, box_h-2]),

        // Switch hole
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, 2],
            radiusStart: switch_d/2+0.1,
            radiusEnd: switch_d/2+0.1,
            resolution: 16}).rotateX(90).rotateZ(90).translate([0, switch_offset_y + 5, switch_offset_z+box_h*0.2]),
        cube({
            center: [0, 0, 0],
            size: [2, switch_d+0.2, switch_offset_z]
        }).translate([0, switch_offset_y+5-switch_d/2-0.1, box_h*0.2]),

        // Wires hole
        CSG.cylinder({
            start: [0, 0, 0],
            end: [0, 0, 2],
            radiusStart: wires_hole_d/2+0.1,
            radiusEnd: wires_hole_d/2+0.1,
            resolution: 16}).rotateX(90).rotateZ(90).translate([0, wires_hole_offset_y, box_h*0.2+wires_hole_d/2]),
        cube({
            center: [0, 0, 0],
            size: [2, wires_hole_d+0.2, wires_hole_d/2]
        }).translate([0, wires_hole_offset_y-wires_hole_d/2-0.1, box_h*0.2]),

        // Board slot
        cube({
            center: [0, 0, 0],
            size: [board_l, board_w, board_h]
        }).translate([5, 5, box_h*0.2])
    );
}

function upper_box() {
    return difference(
        box(),
        cube({
            center: [0, 0, 0],
            size: [box_l, box_w, box_h],
        }).translate([0, 0, -box_h*0.80])
    )
}

function lower_box() {
    return difference(
        box(),
        cube({
            center: [0, 0, 0],
            size: [box_l, box_w, box_h],
        }).translate([0, 0, box_h*0.20])
    )
}

// Put that in main to display the open box with isolator inside
function open_box() {
    return union(
        lower_box(),
        upper_box().translate([20, 0, -box_h]).rotateY(180),
        isolator().translate([5,5,box_h*0.2])
    );
}

// Put that in main to display the closed box with isolator inside
function closed_box() {
    return union(
        lower_box(),
        upper_box(),
        isolator().translate([5,5,box_h*0.2])
    );
}

// Put that in main to export the printable model
function printable() {
    return union(
        lower_box(),
        upper_box().translate([20, 0, -box_h]).rotateY(180)
    );
}

function main() {
    return open_box();
}
