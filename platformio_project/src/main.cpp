#include <Arduino.h>

// Hardware constants
#define ANALOG_FACTOR   5.0/1024.0
#define DIVIDER_FACTOR  4.03

#define LED_PIN         3
#define RELAY_PIN       1
#define MOTOR_BAT_CHAN  1
#define AUX_BAT_CHAN    2

enum States {
  OPEN,
  PRE_COUPLED,
  COUPLED,
  PRE_OPEN
};

States state = OPEN;

//Software constants

// Coupling threshold (V)
float coupling_threshold = 14.0f;
// Uncoupling threshold (V)
float uncoupling_threshold = 13.3f;
// Delay before coupling (ms)
unsigned int coupling_delay = 5000;
// Delay before uncoupling (ms)
unsigned int uncoupling_delay = 1000;

void setup()
{
  // Init pins modes
  pinMode(LED_PIN, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);

  // Init relay pin to LOW (OPEN)
  digitalWrite(RELAY_PIN, LOW);

  // Make LED flash during 1s
  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(LED_PIN, LOW);
}

void loop()
{
  static unsigned long start_time = 0;

  unsigned long current_time = millis();
  int raw_voltage = analogRead(MOTOR_BAT_CHAN);
  float converted_voltage = raw_voltage * DIVIDER_FACTOR * ANALOG_FACTOR;

  switch (state) {
    case OPEN:
    if (converted_voltage > coupling_threshold) {
      start_time = current_time;
      state = PRE_COUPLED;
    }
    break;

    case PRE_COUPLED:
    // Wait for <coupling_delay> before couple
    if (converted_voltage > coupling_threshold) {
      if (current_time < start_time + (coupling_delay/2)) {
        // Blink LED
        if (current_time%500 < 100) {
          digitalWrite(LED_PIN, HIGH);
        } else {
          digitalWrite(LED_PIN, LOW);
        }
      } else if (current_time < start_time + coupling_delay) {
        // Blink LED faster
        if (current_time%200 < 100) {
          digitalWrite(LED_PIN, HIGH);
        } else {
          digitalWrite(LED_PIN, LOW);
        }
      } else {
        // Couple batteries
        digitalWrite(LED_PIN, LOW);
        digitalWrite(RELAY_PIN, HIGH);
        state = COUPLED;
      }
    } else {
      // Cancel couple
      digitalWrite(LED_PIN, LOW);
      state = OPEN;
    }
    break;

    case COUPLED:
    if (converted_voltage < uncoupling_threshold) {
      start_time = current_time;
      state = PRE_OPEN;
    }
    break;

    case PRE_OPEN:
    // Wait for <uncoupling_delay> before uncouple
    if (converted_voltage < uncoupling_threshold) {
      if (current_time < start_time + (uncoupling_delay/2)) {
        // Blink LED
        if (current_time%500 < 100) {
          digitalWrite(LED_PIN, HIGH);
        } else {
          digitalWrite(LED_PIN, LOW);
        }
      } else if (current_time < start_time + uncoupling_delay) {
        // Blink LED faster
        if (current_time%200 < 100) {
          digitalWrite(LED_PIN, HIGH);
        } else {
          digitalWrite(LED_PIN, LOW);
        }
      } else {
        // Uncouple batteries
        digitalWrite(LED_PIN, LOW);
        digitalWrite(RELAY_PIN, LOW);
        state = OPEN;
      }
    } else {
      // Cancel uncouple
      digitalWrite(LED_PIN, LOW);
      state = COUPLED;
    }
    break;
  }

  delay(50);
}
